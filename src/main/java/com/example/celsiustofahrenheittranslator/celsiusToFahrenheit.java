package com.example.celsiustofahrenheittranslator;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class celsiusToFahrenheit extends HttpServlet {

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        String cel= request.getParameter("cel");
        PrintWriter pw =response.getWriter();
        double val= Double.valueOf(cel);
        pw.println(val*9/5+32);
    }
}
